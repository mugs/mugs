package main

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
)

func api(r chi.Router) {
	r.Get("/library", func(w http.ResponseWriter, r *http.Request) {
		if b, err := json.Marshal(entities); err == nil {
			w.Write(b)
		} else {
			http.Error(w, err.Error(), 500)
		}
	})
}
