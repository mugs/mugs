var root = document.body;

var library = {};

fetch("api/library")
	.then(response => {
		return response.json();
	})
	.then(data => {
		library = data;
	})
	.catch(err => {
		console.log(err);
	});

setTimeout(function() {
	window.artist = "Demetori";
	m.redraw();
}, 2000);

var mediaInfoComponent = {
	view: function() {
		return m(
			"div",
			{
				class: "media-info"
			},
			[
				m("div", {
					class: "album-art"
				}),
				m(
					"div",
					{
						class: "current-info"
					},
					[
						m(
							"p",
							{
								class: "current-info song"
							},
							"星条旗のピエロ ～ The MadPiero Laughs"
						),
						m(
							"p",
							{
								class: "current-info artist"
							},
							window.artist
						)
					]
				)
			]
		);
	}
};

var mediaControlsComponent = {
	view: function() {
		return m(
			"div",
			{
				class: "media-wrapper"
			},
			[
				m("button", {
					class: "media prev"
				}),
				m("button", {
					class: "media play"
				}),
				m("button", {
					class: "media skip"
				})
			]
		);
	}
};

var progressBarComponent = {
	view: function() {
		m(
			"div",
			{
				class: "progress-wrapper"
			},
			[
				m(
					"div",
					{
						class: "progress-time"
					},
					[
						m(
							"p",
							{
								class: "progress-time-now"
							},
							"00:24"
						),
						m(
							"p",
							{
								class: "progress-time-total"
							},
							"03:24"
						)
					]
				),
				m("progress", {
					class: "progress",
					max: 100,
					value: 25
				})
			]
		);
	}
};

var playbackControlComponent = {
	view: function() {
		m(
			"div",
			{
				class: "playback-control"
			},
			[
				m("button", {
					class: "media shuffle"
				}),
				m("button", {
					class: "media repeat"
				})
			]
		);
	}
};

var navComponent = {
	view: function() {
		return m("nav", {
			class: "navbar"
		});
	}
};

m.render(
	root,
	m("main", [
		m(
			"nav",
			{
				class: "navbar"
			},
			m(navComponent)
		),
		m("button", "A button")
	])
);

m.mount(navComponent, mediaInfoComponent);
m.mount(navComponent, mediaControlsComponent);
m.mount(navComponent, progressBarComponent);
m.mount(navComponent, playbackControlComponent);

