// @license magnet:?xt=urn:btih:3877d6d54b3accd4bc32f8a48bf32ebc0901502a&dn=mpl-2.0.txt MPL-2.0
var sort = require("fast-sort");
window.song = new Audio();
window.song.addEventListener('ended', function () {
	console.log("ended")
	MoveSong(true)
}, false)
window.song.onreadystatechange = () => {
	console.log(window.song.readyState)
}

function fetchLibrary() {
	// access
	// var library = JSON.parse(window.sessionStorage.getItem("library"))
	fetch("api/library")
		.then(response => response.text())
		.then(body => {
			window.sessionStorage.setItem("library", body);
		})
		.catch(err => {
			console.log(err);
		});
}

fetchLibrary();

function nanoToTime(nanosec) {
	let string = ""

	try {
		let date = new Date(nanosec / 1000000);
		string = date.toISOString().substr(11, 8);
	} catch (e) {
		return "00:00:00"
	}

	return string
}

function buildBox(content, customclass, custom) {
	let classname = "box";
	if (customclass) {
		classname += " " + customclass;
	}

	if (!custom) custom = "";

	return `<div class="${classname}" ${custom}>${content}</div>`;
}

/* REFERENCE
{
  "uuid": "658f2c9ae91c893934642b79ebbbe3a0",
  "filename": "01. イントロ.flac",
  "path": "/home/diamond/Music/Library/2010 - 少女地獄/01. イントロ.flac",
  "mod_time": "2018-10-11T15:54:57-07:00",
  "metadata": {
	"title": "イントロ",
	"album": "少女地獄",
	"artist": "牛肉",
	"genre": "Game",
	"year": 2010,
	"track": 1,
	"comment": "ExactAudioCopy v0.99pb5",
	"length": 68000000000
  }
}
*/
function buildSongEntry(libraryobj, hl) {
	let metadata = libraryobj.metadata;
	var tds = new Array();

	let hlclass = ""
	if (hl) {
		hlclass = " focus"
	}

	tds.push(`<tr class="songs${hlclass}" id="${libraryobj.uuid}" onclick="playSong(this.id)">`)
	tds.push(`<td class="song track">${metadata.track}</td>`);
	tds.push(`<td class="song title">${metadata.title}</td>`);
	tds.push(`<td class="song artist">${metadata.artist}</td>`);
	tds.push(`<td class="song album">${metadata.album}</td>`);
	tds.push(`<td class="song length">${nanoToTime(metadata.length)}</td>`);
	tds.push(`</tr>`)

	return tds.join("\n");
}

setInterval(function () {
	bottomnav.setData({
		currentTime: window.song.currentTime * 1000000000
	})
}, 750)

function playSong(uuid) {
	var data = songlist.getData();

	for (var key in data.library) {
		if (data.library[key].uuid == uuid) {
			let m = data.library[key].metadata

			bottomnav.data.current = {
				UUID: uuid,
				Playing: true, // false/true
				Song: m.title,
				Artist: m.artist,
				Time: 0, // non-formatted
				Length: m.length, // none-formatted
				Shuffle: bottomnav.data.current.Shuffle, // false/true
				Repeat: bottomnav.data.current.Repeat, // "", "all", "one"
			}

			break
		}
	}

	window.song.src = "serve/" + uuid
	window.song.load()
	window.song.play()

	bottomnav.render()
}

// library, sortType (title, album) and reverse bool
function sortLibrary(library, sortType, rev) {
	if (!sortType || typeof sortType !== "string") return library;

	let entries = new Array();
	for (var key in library) {
		entries.push(library[key]);
	}

	let type = "asc"
	if (rev) {
		type = "desc";
	}

	switch (sortType) {
		case "track":
		case "title":
		case "album":
		case "artist":
		case "length":
			sort(entries)[type](u => u.metadata[sortType]);
			break;
	}

	return entries;
}

function sortAndRedraw(type) {
	if (!window.reverse) window.reverse = true;
	else window.reverse = false;

	let entries = sortLibrary(songlist.data.library, type, window.reverse);

	songlist.setData({
		library: Object.assign({}, entries)
	});
}

function playPause() {
	if (window.song.paused) {
		window.song.play()
	} else {
		window.song.pause()
	}

	bottomnav.data.current.Playing = !window.song.paused
	bottomnav.render()
}

// MoveSong() true - next / false - prev
function MoveSong(bool) {
	let current = bottomnav.data.current,
		sngList

	switch (app.data.activeTop) {
		case "Songs":
			sngList = document.querySelectorAll("tr.songs")

			for (key = 0; key < sngList.length; key++) {
				if (sngList[key].id == current.UUID) {
					switch (bool) {
						case true:
							if (key + 1 > sngList.length) {
								key = -1
							}

							key++

							break
						case false:
							if (window.song.currentTime < 5) {
								if (key - 1 < 0) {
									key = sngList.length
								}

								key--
							}

							break
					}

					playSong(sngList[key].id)

					return
				}
			}

			break
		case "Artists":
			sngList = JSON.parse((window.sessionStorage.getItem("artists-songlist")))

			for (var key in sngList) {
				if (sngList[key].uuid == current.UUID) {
					switch (bool) {
						case true:
							if (key + 1 > sngList.length) {
								key = -1
							}

							key++

							break
						case false:
							if (window.song.currentTime < 5) {
								if (key - 1 < 0) {
									key = sngList.length
								}

								key--
							}

							break
					}

					playSong(sngList[key].uuid)

					return
				}
			}

			break
	}
}

var app = new Reef("#app", {
	data: {
		activeTop: "Songs",
	},
	template: function (props) {
		return `<nav id="topnav"></nav>\n<div id="${props.activeTop}"></div>\n<nav id="bottombar"></nav>`
	}
});

var topnav = new Reef("#topnav", {
	template: function (props) {
		var elements = new Array(),
			buttons = new Map()
			.set("Songs", "nav songs")
			.set(`Artists`, "nav artists")
			.set(`Album`, "nav album");

		buttons.forEach(function (v, k) {
			let cl = "box " + v;

			if (app.data.activeTop === k) 
				cl += ` active`;

			elements.push(`<div class="${cl}" onclick="app.setData({activeTop: '${k}'})">${k}</div>`);
		});

		return elements.join("\n");
	},
	attachTo: [app]
});

var artistlist = new Reef("#Artists", {
	template: function (props) {
		let Artists = new Map(),
			library = songlist.data.library

		for (var key in library) {
			let artist = library[key].metadata.artist

			Artists.set(artist, "")
		}

		let artistArray = new Array()
		for (let [key, value] of Artists) {
			artistArray.push(key)
		} 

		sort(artistArray).asc();

		let elements = new Array()
		for (let key of artistArray) {
			elements.push(`
				<div class="artist-tile" onclick="loadArtist('${encodeURI(key)}')"><div class="album-art"></div>${key}</div>
			`)
		}

		return elements.join("\n")
	},
	attachTo: [app]
})

function loadArtist(Artist) {
	let lib = songlist.data.library,
		songs = new Array(),
		artistString = decodeURI(Artist)

	for (var key in lib) {
		if (lib[key].metadata.artist === artistString) {
			songs.push(lib[key])
		}
	}

	window.sessionStorage.setItem("artists-songlist", JSON.stringify(songs))

	playSong(songs[0].uuid)
}

var songlist = new Reef("#Songs", {
	data: {
		library: JSON.parse(window.sessionStorage.getItem("library")),
	},
	template: function (props) {
		console.log(app.data.activeTop)
		var elements = `<table>
			<thead>
				<tr>
					<th class="song track"  onclick="sortAndRedraw('track')" >Track</th>
					<th class="song title"  onclick="sortAndRedraw('title')" >Title</th>
					<th class="song artist" onclick="sortAndRedraw('artist')">Artist</th>
					<th class="song album"  onclick="sortAndRedraw('album')" >Album</th>
					<th class="song length" onclick="sortAndRedraw('length')">Length</th>
				</tr>
			</thead>
			<tbody>
		`

		for (var key in props.library) {
			elements += buildSongEntry(props.library[key], (props.library[key].uuid == bottomnav.data.current.UUID))
		}

		return elements + "</tbody></table>"
	},
	attachTo: [app]
})

var bottomnav = new Reef("#bottombar", {
	data: {
		currentTime: 0,
		current: {
			UUID: "",
			Playing: false, // false/truee
			Song: "",
			Artist: "",
			Length: 0, // none-formatted
			Shuffle: false, // false/true
			Repeat: "", // "", "all", "one"
		}
	},
	template: function (props) {
		return `<div id="bottombar">
					<div class="media-wrapper">
						<button class="media prev" onclick="MoveSong(false)"></button>
						<button 
							class="media play ${props.current.Playing}"
							onclick="playPause()"
						></button>
						<button class="media skip" onclick="MoveSong(true)"></button>
					</div>

					<div class="media-info">
						<div class="album-art"></div>
						<div class="current-info">
							<p class="current-info song">${props.current.Song}</p>
							<p class="current-info artist">${props.current.Artist}</p>
						</div>
					</div>

					<div class="progress-wrapper">
						<div class="progress-time">
							<p class="progress-time-now">
								${nanoToTime(props.currentTime)}
							</p>
							<p class="progress-time-total">
								${nanoToTime(props.current.Length)}
							</p>
						</div>
						<progress class="progress"
									max="${props.current.Length/1000000}"
								  value="${props.currentTime/1000000}"
						/>
					</div>

					<div class="playback-control">
						<button class="media shuffle ${props.current.Shuffle}"></button>
						<button class="media repeat ${props.current.Repeat}"></button>
					</div>
				</div>`
	},
	attachTo: [app]
})

document.addEventListener('render', function (event) {
	if (event.target.matches('#app')) {
		if (app.data.activeTop == "Songs")
			songlist.render()
	}
}, false);

app.render()

// @license-end
