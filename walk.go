package main

import (
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"

	taglib "github.com/Sacules/go-taglib"
)

var (
	// FileTypes contains known file types
	FileTypes = []string{
		"mp3",
		"flac",
	}
)

// Walk .
func Walk() {
	semaphore := make(chan struct{}, 8)

	if err := filepath.Walk(Root, func(path string, info os.FileInfo, err error) error {
		return walkfn(path, info, err, &semaphore)
	}); err != nil {
		panic(err)
	}

	sort.Slice(entities, func(i, j int) bool {
		return entities[i].Metadata.Track < entities[j].Metadata.Track
	})

	sort.SliceStable(entities, func(i, j int) bool {
		return entities[i].Metadata.Album < entities[j].Metadata.Album
	})
}

func walkfn(path string, info os.FileInfo, err error, sema *chan struct{}) error {
	if info == nil {
		return nil
	}

	if info.IsDir() {
		return nil
	}

	for _, ft := range FileTypes {
		if strings.HasSuffix(path, ft) {
			goto Continue
		}
	}

	return nil

Continue:
	walkWG.Add(1)

	go func(path string, info os.FileInfo) {
		*sema <- struct{}{}
		defer func() {
			<-*sema
		}()

		metadata, err := taglib.Read(path)
		if err != nil {
			log.Fatalln(path, err)
			return
		}

		defer metadata.Close()

		entities = append(entities, &Entity{
			UUID:    GenUUID(path),
			Name:    info.Name(),
			Path:    path,
			Size:    info.Size(),
			ModTime: info.ModTime(),
			Metadata: EntryMetadata{
				Title:   metadata.Title(),
				Album:   metadata.Album(),
				Artist:  metadata.Artist(),
				Genre:   metadata.Genre(),
				Year:    metadata.Year(),
				Track:   metadata.Track(),
				Comment: metadata.Comment(),
				Length:  metadata.Length(),
			},
		})
	}(path, info)

	return nil
}
