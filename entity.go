package main

import (
	"time"
)

// Entity is a file thing
type Entity struct {
	UUID     string        `json:"uuid"`
	Name     string        `json:"name"`
	Path     string        `json:"path"`
	Size     int64         `json:"size"`
	ModTime  time.Time     `json:"mod_time"`
	Metadata EntryMetadata `json:"metadata"`
}

type EntryMetadata struct {
	Title   string        `json:"title"`
	Album   string        `json:"album"`
	Artist  string        `json:"artist"`
	Genre   string        `json:"genre"`
	Year    int           `json:"year"`
	Track   int           `json:"track"`
	Comment string        `json:"comment"`
	Length  time.Duration `json:"length"`
}

func GetEntityFromUUID(uuid string) (*Entity, bool) {
	for _, ent := range entities {
		if ent.UUID == uuid {
			return ent, true
		}
	}

	return nil, false
}
