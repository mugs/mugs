package main

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"log"
	"os/exec"
	"strconv"
	"strings"
	"sync"

	"github.com/davecgh/go-spew/spew"
)

const (
	// BitRate for audio
	BitRate uint = 96000

	// VBR controls variable bitrate
	VBR string = "on"

	// CompressionLvl controls compression level, 10 default
	CompressionLvl uint = 10

	// Codec sets the codec, default libopus (heavily recommended)
	Codec string = "libopus"
)

type Progress string

const (
	// ProgressContinue is printed when encoding
	ProgressContinue Progress = "continue"

	// ProgressEnd is called when encoding is finished
	ProgressEnd Progress = "end"

	ProgressUnknown Progress = "?"
)

type FFMpegProgress struct {
	BitRate    string
	TotalSize  int64
	OutTimeus  int64
	OutTimems  int64
	OutTime    string
	DupFrames  uint64
	DropFrames uint64
	Speed      float64
	Percentage float64
	Progress   Progress
}

// FFMpegCommand is the command that's used to transcode music
func FFMpegCommand(input, path string) []string {
	return []string{
		"ffmpeg", "-y",
		"-i", input,
		"-acodec", Codec,
		"-b:a", fmt.Sprintf("%d", BitRate),
		"-vbr", VBR,
		"-compression_level", fmt.Sprintf("%d", CompressionLvl),
		"-progress", "pipe:stdout",
		path,
	}
}

// GenUUID safely
func GenUUID(path string) string {
	return fmt.Sprintf(
		// md5 so it's consistent, even in the database
		// also pretty performant
		"%x", md5.Sum(
			[]byte(path),
		),
	)
}

// Encode runs the file through ffmpeg
func (e *Entity) Encode(path string, prCh chan FFMpegProgress) {
	file := e.Path

	cmd := exec.Command(
		"/usr/bin/env", FFMpegCommand(file, path)...,
	)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatalln(err)
		return
	}

	scanner := bufio.NewScanner(stdout)

	// print("\033[2J\033[1;1H")
	// w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.TabIndent)

	syncwg := sync.WaitGroup{}
	syncwg.Add(1)

	defer syncwg.Wait()

	go func() {
		defer stdout.Close()
		defer syncwg.Done()

		progress := FFMpegProgress{}

		for scanner.Scan() {
			if kv := strings.Split(scanner.Text(), "="); len(kv) == 2 {
				switch kv[0] {
				case "bitrate":
					// print("\033[2J\033[1;1H")
					progress.BitRate = strings.TrimSpace(kv[1])
				case "total_size":
					size, _ := strconv.ParseInt(kv[1], 10, 64)
					progress.TotalSize = size
				case "out_time_us":
					us, _ := strconv.ParseInt(kv[1], 10, 64)
					progress.OutTimeus = us
				case "out_time_ms":
					ms, _ := strconv.ParseInt(kv[1], 10, 64)
					progress.OutTimems = ms
				case "out_time":
					progress.OutTime = kv[1]
				case "dup_frames":
					dup, _ := strconv.ParseUint(kv[1], 10, 64)
					progress.DupFrames = dup
				case "drop_frames":
					drop, _ := strconv.ParseUint(kv[1], 10, 64)
					progress.DropFrames = drop
				case "speed":
					speed, _ := strconv.ParseFloat(
						strings.TrimSuffix(
							strings.TrimSpace(kv[1]),
							"x",
						),
						64,
					)

					progress.Speed = speed
				case "progress":
					switch kv[1] {
					case string(ProgressContinue):
						progress.Progress = ProgressContinue
					case string(ProgressEnd):
						progress.Progress = ProgressEnd
					default:
						progress.Progress = ProgressUnknown
						log.Println("Unknown progress:", kv[1])
					}

					prCh <- progress // push Progress to channel

					if progress.Progress == ProgressEnd {
						close(prCh)
					}
				}
			} else {
				log.Println(scanner.Text())
			}
		}

		if err := scanner.Err(); err != nil {
			log.Println(err)
		}
	}()

	if err := cmd.Start(); err != nil {
		log.Fatalln(err)
		return
	}

	if err := cmd.Wait(); err != nil {
		log.Fatalln(err)
		spew.Dump(cmd.Stderr)
		return
	}

	return
}
