package main

import (
	"io"
	"net/http"
)

func copyBuffer(dst http.ResponseWriter, src io.Reader, progress chan FFMpegProgress) (written int64, err error) {
	size := 1024
	if l, ok := src.(*io.LimitedReader); ok && int64(size) > l.N {
		if l.N < 1 {
			size = 1
		} else {
			size = int(l.N)
		}
	}

	buf := make([]byte, size)

	isFFmpegOk := true

MainLoop:
	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}

			if ew != nil {
				err = ew
				break MainLoop
			}

			if nr != nw {
				err = io.ErrShortWrite
				break MainLoop
			}
		}

		if er != nil && er != io.EOF {
			return written, er
		}

		if er == io.EOF && !isFFmpegOk {
			return written, nil
		}

		select {
		case prCh, ok := <-progress:
			if !ok {
				isFFmpegOk = false
			}

			if prCh.Progress != ProgressContinue {
				isFFmpegOk = false
			}
		default:
		}
	}

	return written, err
}
